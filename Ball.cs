﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Games {
    class Ball: Node {
        public float Radius {get; set;}

        // directional vector
        public float DirX {get; set;}
        public float DirY {get; set;} 

        public float Speed {get; set;}

        public Ball() {
            Radius = 5;
            DirX = 0;
            DirY = 0;
            Speed = 250;
        }

        public override Box ComputeBoundingBox() {
            return new Box(GlobalX - Radius, GlobalY - Radius, 2 * Radius, 2 * Radius);
        }

        public void normalizeDir() {
            double f = 1.0 / Math.Sqrt(DirX * (double)DirX + DirY * (double)DirY);
            if (Double.IsInfinity(f) || Double.IsNaN(f)) {
                f = 0.0;
            }
            DirX = (float)(DirX * f);
            DirY = (float)(DirY * f);
        }

        public override void Draw(Graphics g) {
            g.FillEllipse(Brushes.Red, GlobalX - Radius, GlobalY - Radius, 2 * Radius, 2 * Radius);
        }

        public override void Update(double deltaT) {
            GlobalX = (float)(GlobalX + DirX * Speed * deltaT);
            GlobalY = (float)(GlobalY + DirY * Speed * deltaT);
        }
    }
}
