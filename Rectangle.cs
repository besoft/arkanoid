﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Games {
    class Box {
        float x, y, width, height;

        public Box(float x, float y, float width, float height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        public float X { get { return x; }}
        public float Y { get { return y; } }
        public float Width { get { return width; } }
        public float Height { get { return height; } }

        // AABB collision test
        public bool testCollision(Box other) {
            bool collisionX = this.X + this.Width  >= other.X && other.X + other.Width >= this.X;
            bool collisionY = this.Y + this.Height >= other.Y && other.Y + other.Height >= this.Y;
            return collisionX && collisionY;
        }
    }
}
