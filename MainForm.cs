﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Games {
    public partial class MainForm : Form {
        public MainForm() {
            InitializeComponent();

            ClientSize = new Size(WIDTH, HEIGHT);
            
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            DoubleBuffered = true;

            SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);

            // Retrieves the BufferedGraphicsContext for the 
            // current application domain.
            context = BufferedGraphicsManager.Current;

            // Sets the maximum size for the primary graphics buffer
            // of the buffered graphics context for the application
            // domain.  Any allocation requests for a buffer larger 
            // than this will create a temporary buffered graphics 
            // context to host the graphics buffer.
            context.MaximumBuffer = new Size(ClientSize.Width + 1, ClientSize.Height + 1);

            // Allocates a graphics buffer the size of this form
            // using the pixel format of the Graphics created by 
            // the Form.CreateGraphics() method, which returns a 
            // Graphics object that matches the pixel format of the form.
            grafx = context.Allocate(CreateGraphics(),
                 new Rectangle(0, 0, ClientSize.Width, ClientSize.Height));

            FormClosing += MainForm_FormClosing;
            Shown += MainForm_Shown;

            game = new Game(ClientSize.Width, ClientSize.Height);
            KeyUp += game.OnKeyUp;
            KeyDown += game.OnKeyDown;
        }
    }
}
