﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Games {
    class Util {
        public static float Clamp(float x, float min, float max) {
            return Math.Max(Math.Min(x, max), min);
        }

        public static void reflect(float nx, float ny, ref float vx, ref float vy) {
            double twice_nv = 2 * (nx * vx + ny * vy);
            vx = (float)(twice_nv * nx - vx);
            vy = (float)(twice_nv * ny - vy);
        }

        public static double norm(float vx, float vy) {
            return Math.Sqrt(vx * (double)vx + vy * (double)vy);
        }

        public static void normalize(ref float vx, ref float vy) {
            double f = 1.0 / norm(vx, vy);
            vx = (float)(vx * f);
            vy = (float)(vy * f);
        }

        public bool pointInCircle(float x, float y, float cx, float cy, float r) {
            double dx = x - cx, dy = y - cy;
            return dx * dx + dy * dy <= r * r;
        }
    }
}
