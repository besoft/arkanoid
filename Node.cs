﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Games {
    class Node {
        private float x, y;

        public Node Parent { get; set; }

        public float X { get { return x; } set { x = value; } }
        public float Y { get { return y; } set { y = value; } }

        public float GlobalX {
            get { return (Parent != null ? Parent.GlobalX : 0.0f) + X; }
            set { x = value - (Parent != null ? Parent.GlobalX : 0.0f); }
        }

        public float GlobalY { 
            get { return (Parent != null ? Parent.GlobalY : 0.0f) + Y; }
            set { y = value - (Parent != null ? Parent.GlobalY : 0.0f); }
        }

        public virtual Box ComputeBoundingBox() {
            return new Box(GlobalX, GlobalY, 0, 0);
        }

        public virtual void Update(double deltaT) {
        }

        public virtual void Draw(Graphics g) {
        }
    }
}
