﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Games {
    class Paddle : Node {
        bool flag = false;
        private float radius = 50;
        private float height = 10;
        private float speed = 300;

        public float Radius { get { return radius; } }
        public float Height { get { return height; } }
        public float Speed  { get { return speed; } }

        public Paddle() {
        }

        public override void Draw(Graphics g) {
            g.FillRectangle(flag ? Brushes.Red : Brushes.White, GlobalX - Radius, GlobalY, 2 * Radius, Height);
        }

        public override Box ComputeBoundingBox() { 
            float gx = GlobalX, gy = GlobalY, r = Radius;
            return new Box(gx - r, gy, 2 * r, Height);
        }
    }
}
