﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace Games {
    class Game {
        private const int LIVES = 5;

        int width, height;
        Random rnd = new Random();
        Paddle paddle = new Paddle();
        Ball ball = new Ball();
        List<Brick> bricks = new List<Brick>();
        int lives = LIVES;

        bool movingLeft, movingRight, performingAction;

        public Game(int width, int height) {
            this.width = width;
            this.height = height;

            ResetGame();
        }

        public void ResetGame() {
            lives = LIVES;
            ResetPaddle();
            ResetBricks();
            ResetBall();
        }

        public void ResetPaddle() {
            paddle.X = width / 2; // in the middle
            paddle.Y = height - paddle.Height;     // at the bottom
        }

        public void ResetBall() {
            ball.Parent = paddle;
            ball.DirX = 0;
            ball.DirY = 0;
            ball.X = 0;
            ball.Y = -ball.Radius;
        }

        public void ResetBricks() {
            bricks.Clear();
            float w = 50, h = 20;
            for (int row = 3; row < 10; ++row) {
                float sx = 0;
                while (sx + w < width + w / 2) {
                    Brick b = new Brick();
                    b.Width = w;
                    b.Height = h;
                    b.GlobalX = sx;
                    b.GlobalY = row * h;
                    bricks.Add(b);
                    sx += w;
                }
            }
        }

        public void OnKeyDown(object sender, KeyEventArgs e) {
            movingLeft  |= e.KeyCode == Keys.Left;
            movingRight |= e.KeyCode == Keys.Right;
            performingAction |= e.KeyCode == Keys.Space;
        }

        public void OnKeyUp(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Left) {
                movingLeft = false;
            }
            if (e.KeyCode == Keys.Right) {
                movingRight = false;
            }
            if (e.KeyCode == Keys.Space) {
                performingAction = false;
            }
        }

        public void Update(double deltaT) {
            // Update the paddle position
            int dx = 0;
            dx -= movingLeft ? 1 : 0;
            dx += movingRight ? 1 : 0;
            paddle.GlobalX = Util.Clamp((float)(paddle.GlobalX + dx * paddle.Speed * deltaT), paddle.Radius, Math.Max(width - paddle.Radius, 0));

            float gx, gy;
            if (ball.Parent == paddle) {
                if (performingAction) {
                    // Give a random direction to the ball
                    gx = ball.GlobalX;
                    gy = ball.GlobalY;
                    ball.Parent = null;
                    ball.X = gx;
                    ball.Y = gy;

                    ball.DirY = -1;
                    ball.DirX = (float)(2 * rnd.NextDouble() - 1);
                    ball.normalizeDir();
                }
            } else {
                // Update the ball position
                bool ballCollision = false;
                float prevX = ball.X, prevY = ball.Y;
                ball.Update(deltaT);
                gx = ball.GlobalX;
                gy = ball.GlobalY;
                if (gy - ball.Radius >= height) { // The ball is outside the guarded area
                    Die();
                } else if (gx - ball.Radius <= 0 || gx + ball.Radius >= width) { // Bounce from the left or right wall
                    ballCollision = true; 
                    ball.DirX = -ball.DirX;
                } else if (gy - ball.Radius <= 0) { // Bounce from the top wall
                    ballCollision = true;
                    ball.DirY = -ball.DirY;
                } else {
                    double dist = gx - paddle.GlobalX;
                    if (gy + ball.Radius >= height - paddle.Height && Math.Abs(dist) <= paddle.Radius) { // Bounce from the paddle
                        ballCollision = true;
                        // Ordinary bounce
                        if (Math.Abs(dist) * 6 < paddle.Radius) {
                            ball.DirY = -ball.DirY;
                        }
                        // Modified bounce
                        else {
                            float m = (float)(Math.Abs(dist) * 3.0 / paddle.Radius - 0.5) * (dist < 0 ? -1 : +1);
                            float nx = 0.3f * m, ny = -1;
                            float vx = -ball.DirX, vy = -ball.DirY;
                            Util.normalize(ref nx, ref ny);
                            Util.reflect(nx, ny, ref vx, ref vy);
                            ball.DirX = vx;
                            ball.DirY = vy > -0.3f ? -0.3f : vy;
                            ball.normalizeDir();
                        }
                    } else {
                        // ball vs. bricks
                        Brick brick = FindCollidingBrick();
                        if (brick != null) {
                            OnHit(brick);

                            ball.DirY = -ball.DirY;
                            ball.DirX = -ball.DirX;
                            ballCollision = true;
                        }
                    }
                }
                if (ballCollision) {
                    ball.X = prevX;
                    ball.Y = prevY;
                }
            }
        }

        private Brick FindCollidingBrick() {
            Box ballBox = ball.ComputeBoundingBox();
            foreach (Brick b in bricks) {
                Box brickBox = b.ComputeBoundingBox();
                if (ballBox.testCollision(brickBox)) {
                    return b;
                }
            }
            return null;
        }

        private void OnHit(Brick b) {
            bricks.Remove(b);
        }

        private void Die() {
            lives = lives - 1;
            if (lives >= 1) {
                ResetPaddle();
                ResetBall();
            } else {
                ResetGame();
            }
        }

        public void Draw(Graphics g) {
            g.Clear(Color.Black);

            paddle.Draw(g);

            ball.Draw(g);

            foreach (Brick b in bricks) {
                b.Draw(g);
            }
        }
    }
}
