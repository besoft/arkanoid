﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Games {
    partial class MainForm {
        static int WIDTH = 800, HEIGHT = 600;
		
        private Game game;
        private BufferedGraphicsContext context;
        private BufferedGraphics grafx;

		TimeSpan accumulatedTime;
		TimeSpan lastTime;
        Stopwatch stopwatch = new Stopwatch();
        
        readonly TimeSpan TargetElapsedTime = TimeSpan.FromTicks(TimeSpan.TicksPerSecond / 150);
		readonly TimeSpan MaxElapsedTime = TimeSpan.FromTicks(TimeSpan.TicksPerSecond / 10);
        Timer timer = new Timer();

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
                grafx.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Text = "Form1";
        }

        #endregion

        void MainForm_Shown(Object sender, EventArgs e) {
            Console.WriteLine("Form shown");
            Application.Idle += Tick;
            timer.Interval = (int)TargetElapsedTime.TotalMilliseconds;
			timer.Tick += Tick;
			timer.Start();
            stopwatch.Restart();
        }

        void MainForm_FormClosing(Object sender, FormClosingEventArgs e) {
            Console.WriteLine("Form closing");
        }
        
        // http://www.shawnhargreaves.com/blog/when-winforms-met-game-loop.html
        void Tick(object sender, EventArgs e) {
			TimeSpan currentTime = stopwatch.Elapsed;
			TimeSpan elapsedTime = currentTime - lastTime;
			lastTime = currentTime;

			if (elapsedTime > MaxElapsedTime) {
				elapsedTime = MaxElapsedTime;
			}

			accumulatedTime += elapsedTime;
			
			bool updated = accumulatedTime >= TargetElapsedTime;
			while (accumulatedTime >= TargetElapsedTime) {
				game.Update(TargetElapsedTime.Milliseconds / 1000.0);
				accumulatedTime -= TargetElapsedTime;
			}

            if (updated) {
				grafx.Graphics.Clear(Color.Black);
				game.Draw(grafx.Graphics);
				using (Graphics g = CreateGraphics()) {
					grafx.Render(g);
				}
			}
		}
    }
}

