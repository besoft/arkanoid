﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Games {
    class Brick: Node {
        public float Width { get; set; }
        public float Height { get; set; }

        public Brick() {
        }

        public override Box ComputeBoundingBox() {
            return new Box(GlobalX, GlobalY, Width, Height);
        }

        public override void Draw(Graphics g) {
            g.FillRectangle(Brushes.White, GlobalX, GlobalY, Width, Height);
            g.DrawLine(Pens.DarkGray, GlobalX, GlobalY + Height - 1, GlobalX + Width - 1, GlobalY + Height - 1);
            g.DrawLine(Pens.DarkGray, GlobalX + Width - 1, GlobalY + Height - 1, GlobalX + Width - 1, GlobalY);
        }
    }
}
